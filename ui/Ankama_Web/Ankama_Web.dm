<module>
    <!-- Information about the module -->
    <header>
        <!-- Name displayed in modules list -->
        <name>Web</name>
        
        <!-- Module's version -->
        <version>0.1</version>

        <!-- Last Dofus version that works with -->
        <dofusVersion>2.0</dofusVersion>

        <!-- Author of the module -->
        <author>Ankama</author>

        <!-- A short description -->
        <shortDescription>ui.module.web.shortDesc</shortDescription>

        <!-- Detailled description -->
        <description></description>
	</header>

	<uiGroup name="Web" exclusive="true" permanent="true" />

	<uis group="shop">
		<ui name="webBase" 			file="xml/webBase.xml" 					class="ui::WebBase"/>
		<ui name="webShop"			file="xml/webShop.xml"					class="ui::WebShop" />
		<ui name="shopPopupAttitude" 		file="xml/shopPopupAttitude.xml" 			    class="ui::ShopPopupAttitude"/>
		<ui name="shopPopupCompanion" 		file="xml/shopPopupCompanion.xml" 			    class="ui::ShopPopupCompanion"/>
		<ui name="shopPopupCompanionPack" 		file="xml/shopPopupCompanionPack.xml" 		class="ui::ShopPopupCompanionPack"/>
		<ui name="shopPopupFinishMove" 		file="xml/shopPopupFinishMove.xml" 			    class="ui::ShopPopupFinishMove"/>
		<ui name="shopPopupHarness" 		file="xml/shopPopupHarness.xml" 			    class="ui::ShopPopupHarness"/>
		<ui name="shopPopupHavenBag" 		file="xml/shopPopupHavenBag.xml" 			    class="ui::ShopPopupHavenBag"/>
		<ui name="shopPopupItemSet" 		file="xml/shopPopupItemSet.xml" 			    class="ui::ShopPopupItemSet"/>
		<ui name="shopPopupMysteryBox" 		file="xml/shopPopupMysteryBox.xml" 			    class="ui::ShopPopupMysteryBox"/>
		<ui name="shopPopupLivingObject" 	file="xml/shopPopupLivingObject.xml" 			class="ui::ShopPopupLivingObject"/>
		<ui name="shopPopupOneClickRegister" 	file="xml/shopPopupOneClickRegister.xml" 	class="ui::ShopPopupOneClickRegister"/>
		<ui name="shopPopupOneClickCode" 	file="xml/shopPopupOneClickCode.xml" 	class="ui::ShopPopupOneClickCode"/>
		<ui name="shopPopupOneClickPayment" 	file="xml/shopPopupOneClickPayment.xml" 	class="ui::ShopPopupOneClickPayment"/>
		<ui name="shopPopupPack" 	file="xml/shopPopupPack.xml" 			class="ui::ShopPopupPack"/>
		<ui name="shopPopupConfirmBuy" 	file="xml/shopPopupConfirmBuy.xml" 			class="ui::ShopPopupConfirmBuy"/>
		<ui name="webBak" 			file="xml/webBak.xml" 					class="ui::WebBak"/>
		<ui name="mysteryBoxPopup"  file="xml/mysteryBoxPopup.xml"          class="ui::MysteryBoxPopup"/>
		<ui name="codeResultPopup"  file="xml/codeResultPopup.xml"          class="ui::CodeResultPopup"/>
		<ui name="webCodesAndGifts"  file="xml/webCodesAndGifts.xml"          class="ui::WebCodesAndGifts"/>
	</uis>
	
	<uis group="social">
		<ui name="sharePopup" 		file="xml/sharePopup.xml" 			class="ui::SharePopup"/>
	</uis>
    
	<script>Web.swf</script>
    
</module>