<module>
    <!-- Information about the module -->
    <header>
        <!-- Name displayed in modules list -->
        <name>ContextMenu</name>
        
        <!-- Module's version -->
        <version>0.1</version>

        <!-- Last Dofus version that works with -->
        <dofusVersion>2.0</dofusVersion>

        <!-- Author of the module -->
        <author>Ankama</author>

        <!-- A short description -->
        <shortDescription>ui.module.contextmenu.shortDesc</shortDescription>

        <!-- Detailled description -->
        <description></description>
	</header>
    <uis>
        <ui name="contextMenu" file="ui/contextMenu.xml" class="ui::ContextMenuUi" />
    </uis>
    
    <cachedFiles>
        <path>css</path>
    </cachedFiles>

    <script>ContextMenu.swf</script>
</module> 