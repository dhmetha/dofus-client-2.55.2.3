<module>
    <!-- Information about the module -->
    <header>
        <!-- Name displayed in modules list -->
        <name>Console</name>
        
        <!-- Module's version -->
        <version>0.1</version>

        <!-- Last Dofus version that works with -->
        <dofusVersion>2.0</dofusVersion>

        <!-- Author of the module -->
        <author>Ankama</author>

        <!-- A short description -->
        <shortDescription>ui.module.console.shortDesc</shortDescription>

        <!-- Detailled description -->
        <description></description>
	</header>
    <uis>
        <ui name="console" file="console.xml" class="ui::ConsoleUi" />
    </uis>
    
    <shortcuts>shortcuts.xml</shortcuts>
    
    <script>Console.swf</script>
    
</module> 