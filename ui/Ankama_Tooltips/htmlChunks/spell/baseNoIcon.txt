<Definition useCache="false">
	<Constants>
		<Constant name="spellZoneIconY" value="56" type="int" />
		<Constant name="spellZoneIconNoCriticalY" value="38" type="int" />
		<Constant name="spellZoneIconY-smallScreen" value="66" type="int" />
		<Constant name="spellZoneIconNoCriticalY-smallScreen" value="45" type="int" />
	</Constants>
	
	<Container name="mainCtr">
		<Texture name="tx_spellZoneIcon">
			<Anchors>
				<Anchor>
					<AbsDimension x="0" y="[local.spellZoneIconY]" />
				</Anchor>
			</Anchors>
		</Texture>
		
		<Label name="lbl_content">
			<Size>
				<AbsDimension x="386"/>
			</Size>
			<css>[config.ui.skin]css/tooltip_spell.css</css>
			<useCustomFormat>true</useCustomFormat>
			<wordWrap>true</wordWrap>
			<fixedWidth>false</fixedWidth>
			<hyperlinkEnabled>true</hyperlinkEnabled>
		</Label>
	</Container>
</Definition>