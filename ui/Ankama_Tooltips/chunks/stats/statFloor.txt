<Container>
	<Label>
		<Anchors>
			<Anchor>
				<AbsDimension x="0" y="0"/>
			</Anchor>
		</Anchors>
		<Size>
			<AbsDimension x="100" y="20"/>
		</Size>
		<text>#floor</text>
		<cssClass>center</cssClass>
		<css>[local.css]normal2.css</css>
	</Label>
	<Label>
		<Anchors>
			<Anchor relativeTo="$LAST" relativePoint="TOPRIGHT">
				<AbsDimension x="70" y="0" />
			</Anchor>
		</Anchors>
		<text>#statInterval</text>
		<cssClass>left</cssClass>
		<css>[local.css]normal2.css</css>
		<fixedWidth>false</fixedWidth>
	</Label>
	<Label>
		<Anchors>
			<Anchor relativeTo="$LAST" relativePoint="TOPRIGHT">
				<AbsDimension x="70" y="0" />
			</Anchor>
		</Anchors>
		<text>#statCost</text>
		<cssClass>left</cssClass>
		<css>[local.css]normal2.css</css>
		<fixedWidth>false</fixedWidth>
	</Label>
</Container>