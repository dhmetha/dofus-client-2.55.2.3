<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE Definition SYSTEM "../dtd/ui.dtd">
<Definition useCache="false" debug="false">
	<Constants>
		<!-- Generic path -->
		<Constant name="hud" value="[config.ui.skin]texture/hud/" />
		<Constant name="assets" value="[config.ui.skin]assets.swf|" />
		<Constant name="css" value="[config.ui.skin]css/" />
	</Constants>

	<Import url="[config.mod.template]button/themeIconButton.xml" />

	<Container name="mainCtr">
		#content

		<Container name="backgroundCtr" strata="LOW">
			<Anchors>
				<Anchor relativePoint="TOPLEFT">
					<AbsDimension x="-20" y="-10" />
				</Anchor>
				<Anchor relativePoint="BOTTOMRIGHT">
					<AbsDimension x="2" y="5" />
				</Anchor>
			</Anchors>
			<shadowColor>[colors.ui.shadow]</shadowColor>
			<bgCornerRadius>10</bgCornerRadius>
			<bgColor>[colors.tooltip.bg]</bgColor>
			<bgAlpha>[colors.tooltip.bg.alpha]</bgAlpha>
			<borderColor>[colors.tooltip.border]</borderColor>
		</Container>

		<themeIconButton name="btnClose">
            <iconUri>[config.ui.skin]texture/btnIcon/btnIcon_cross.png</iconUri>
            <smooth>true</smooth>
            <visible>false</visible>
        </themeIconButton>
	</Container>
</Definition>