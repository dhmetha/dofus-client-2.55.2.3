<!-- info sur une ligne complete -->
<Label>
	<text>#text</text>
	<Anchors>
		<Anchor relativeTo="$LAST" relativePoint="BOTTOMLEFT" />
	</Anchors>
	<Size>
		<AbsDimension x="412" y="15" />
	</Size>
	<cssClass>#cssClass</cssClass>
	<css>[local.css]tooltip_default.css</css>
</Label>