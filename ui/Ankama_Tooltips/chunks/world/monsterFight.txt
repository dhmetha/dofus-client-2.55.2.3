<Label name="lbl_name">
	<autoResize>false</autoResize>
	<fixedWidth>false</fixedWidth>
	<cssClass>center</cssClass>
	<css>[local.css]tooltip_title.css</css>
</Label>

	<!-- Utilisé pour niveau en preparation de combat -->
<Label name="lbl_info">
	<autoResize>false</autoResize>
	<css>[local.css]normal2.css</css>
	<cssClass>center</cssClass>
</Label>

<Label name="lbl_damage">
	<css>[local.css]normal2.css</css>
	<cssClass>center</cssClass>
	<fixedWidth>false</fixedWidth>
</Label>

<Label name="lbl_fightStatus">
	<autoResize>false</autoResize>
	<css>[local.css]normal2.css</css>
</Label>