<!-- ALIGNEMENT -->

<Container>
	<Anchors>
		<Anchor relativeTo="infosCtr" relativePoint="TOP">
			<AbsDimension x="0" y="-5" />
		</Anchor>
	</Anchors>
	<Size>
		<AbsDimension x="0" y="0" />
	</Size>
	<Texture name="tx_alignment" />
</Container>

<Container>
	<Anchors>
		<Anchor relativeTo="infosCtr" relativePoint="BOTTOM">
			<AbsDimension x="0" y="-5" />
		</Anchor>
	</Anchors>
	<Size>
		<AbsDimension x="0" y="0" />
	</Size>
	<Texture name="tx_alignmentBottom" />
</Container>

<Container name="infosCtr">
	
	<Label name="lbl_guildName">
		<Anchors>
			<Anchor>
				<AbsDimension x="50" y="0" />
			</Anchor>
		</Anchors>
		<text>#guildName</text>
		<fixedWidth>false</fixedWidth>
		<css>[local.css]normal2.css</css>
	</Label>

	<Label name="lbl_playerName">
		<Anchors>
			<Anchor>
				<AbsDimension x="50" y="18" />
			</Anchor>
		</Anchors>
		<text>#playerName</text>
		<fixedWidth>false</fixedWidth>
		<css>[local.css]tooltip_title.css</css>
	</Label>

	<!-- EMBLEME DE GUILDE -->

	<Texture name="tx_emblemBack">
		<Anchors>
			<Anchor>
				<AbsDimension x="2" y="0" />
			</Anchor>
		</Anchors>
		<Size>
			<AbsDimension x="40" y="40" />
		</Size>
		<visible>false</visible>
	</Texture>
	<Texture name="tx_emblemUp">
		<Anchors>
			<Anchor relativeTo="tx_emblemBack" relativePoint="CENTER">
				<AbsDimension x="-12" y="-12" />
			</Anchor>
		</Anchors>
		<Size>
			<AbsDimension x="24" y="24" />
		</Size>
		<visible>false</visible>
	</Texture>
	
	<!-- EMBLEME D'ALLIANCE -->
	
	<Texture name="tx_AllianceEmblemBack">
		<Size>
			<AbsDimension x="40" y="40" />
		</Size>
		<visible>false</visible>
	</Texture>
	<Texture name="tx_AllianceEmblemUp">
		<Size>
			<AbsDimension x="24" y="24" />
		</Size>
		<visible>false</visible>
	</Texture>
	
	<Container name="tx_back" strata="LOW">
			<Anchors>
				<Anchor relativePoint="TOPLEFT">
					<AbsDimension x="-5" y="-5" />
				</Anchor>
				<Anchor relativePoint="BOTTOMRIGHT">
					<AbsDimension x="5" y="5" />
				</Anchor>
			</Anchors>
			<shadowColor>[colors.ui.shadow]</shadowColor>
			<bgCornerRadius>10</bgCornerRadius>
			<bgColor>[colors.tooltip.bg]</bgColor>
			<bgAlpha>[colors.tooltip.bg.alpha]</bgAlpha>
	</Container>
</Container>