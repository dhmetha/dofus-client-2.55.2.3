<module>
    <!-- Information about the module -->
    <header>
        <!-- Name displayed in modules list -->
        <name>Grimoire</name>

        <!-- Module's version -->
        <version>0.1</version>

        <!-- Last Dofus version that works with -->
        <dofusVersion>2.0</dofusVersion>

        <!-- Author of the module -->
        <author>Ankama</author>

        <!-- A short description -->
        <shortDescription>ui.module.grimoire.shortDesc</shortDescription>

        <!-- Detailled description -->
        <description></description>
	</header>

	<uiGroup name="grimoire" exclusive="true" permanent="false" />

	<uis group="grimoire">
		<ui name="book" 				    file="ui/book.xml" 						        class="ui::Book"/>
		<ui name="alignmentTab"			    file="ui/alignmentTab.xml"				        class="ui::AlignmentTab"/>
		<ui name="bestiaryTab" 			    file="ui/bestiaryTab.xml"				        class="ui::BestiaryTab"	/>
		<ui name="questTab" 			    file="ui/questTab.xml"					        class="ui::QuestTab"/>
		<ui name="jobTab" 				    file="ui/jobTab.xml"					        class="ui::JobTab"/>
		<ui name="calendarTab" 			    file="ui/calendarTab.xml"				        class="ui::CalendarTab"/>
		<ui name="achievementTab" 		    file="ui/achievementTab.xml"			        class="ui::AchievementTab"/>
		<ui name="titleTab" 			    file="ui/titleTab.xml"					        class="ui::TitleTab"/>
		<ui name="companionTab" 		    file="ui/companionTab.xml"				        class="ui::CompanionTab"/>
		<ui name="idolsTab"				    file="ui/idolsTab.xml"					        class="ui::IdolsTab"/>
		<ui name="gameplayClassWindow" 	    file="ui/gameplayClassWindow.xml"	            class="ui::GameplayClassWindow"/>
		<ui name="giftXmlItem"			    file="ui/items/giftXmlItem.xml"			        class="ui.items::GiftXmlItem" />
		<ui name="questBase" 			    file="ui/questBase.xml" 				        class="ui::QuestBase"/>
		<ui name="spellBase" 			    file="ui/spellBase.xml" 				        class="ui::SpellBase"/>
		<ui name="spellList" 			    file="ui/spellList.xml" 				        class="ui::SpellList"/>
		<ui name="finishMoveList"		    file="ui/finishMoveList.xml"			        class="ui::FinishMoveList"/>
		<ui name="encyclopediaBase"		    file="ui/encyclopediaBase.xml"			        class="ui::EncyclopediaBase"/>
		<ui name="encyclopediaList"		    file="ui/encyclopediaList.xml"			        class="ui::EncyclopediaList"/>

		<!-- Temporis III -->
		<ui name="alignmentWarEffortTab"    file="ui/temporis/alignmentWarEffortTab.xml"    class="ui.temporis::AlignmentWarEffortTab"/>
		<ui name="alignmentGiftsTab"        file="ui/temporis/alignmentGiftsTab.xml"        class="ui.temporis::AlignmentGiftsTab"/>

		<!-- Temporis IV -->
		<ui name="temporisSpellsUi"    		file="ui/temporis/temporisSpellsUi.xml"    		class="ui.temporis::TemporisSpellsUi"/>
        <ui name="temporisSpellSetPopUp"    file="ui/temporis/temporisSpellSetPopUp.xml"	class="ui.temporis::TemporisSpellSetPopUp" />
        <ui name="temporisQuestTab"         file="ui/temporis/temporisQuestTab.xml"	        class="ui.temporis::TemporisQuestTab" />
        <ui name="temporisGetScrollWarningPopUp" file="ui/temporis/temporisGetScrollWarningPopUp.xml" class="ui.temporis::TemporisGetScrollWarningPopUp" />
        <ui name="temporisIntroPopUp" 		file="ui/temporis/temporisIntroPopUp.xml" 		class="ui.temporis::TemporisIntroPopUp" />
        <ui name="temporisSpellSetsUi" 		file="ui/temporis/temporisSpellSetsUi.xml" 		class="ui.temporis::TemporisSpellSetsUi" />
        <ui name="temporisSpellSetDeletionPopUp" file="ui/temporis/temporisSpellSetDeletionPopUp.xml" class="ui.temporis::TemporisSpellSetDeletionPopUp" />
	</uis>

	<uis>
		<ui name="questList"			file="ui/questList.xml"					class="ui::QuestList"/>
		<ui name="questListMinimized"	file="ui/questListMinimized.xml"		class="ui::QuestListMinimized"/>
	</uis>

	<script>Grimoire.swf</script>

</module>